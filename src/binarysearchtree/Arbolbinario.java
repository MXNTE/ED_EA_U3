
package binarysearchtree;


//clase Arbol binario 

public class Arbolbinario {


private Arbol root;
//metodo para insercion de llave y valor
public void insert(int key, String value){
    
   //nuevo nodo
Arbol newNode = new Arbol(key, value);

//si la raiz no tiene un valor, se establece a este como la raiz
if(root==null){

root = newNode;
}
else{
Arbol current = root;
Arbol parent;


while(true){
parent = current;
if(key<current.key){
current = current.leftChild;
if(current == null){//su padre es el nodo hoja
    parent.leftChild=newNode; 
    
    return;

}


else{
current=current.rightChild;

if(current==null){
parent.rightChild=newNode;
return;

}


}


}

}


}

}

//encuentra el valor minimo entre los nodos 
    public Arbol findMin(){
    Arbol current = root;
    Arbol last = null;
    
    while(current!=null){
    last = current;
    current = current.leftChild;
    
    
    }
    return last;
    
    
    }



 //encuentra el valor minimo entre los nodos 
    public Arbol findMax(){
    Arbol current = root;
    Arbol last = null;
    
    while(current!=null){
    last = current;
    current = current.leftChild;
    
    
    }
    return last;
    
    
    }
    
    public boolean remove(int key){
    //si el nodo es una hoja
    Arbol currentNode =root;
    Arbol parentNode = root;
    
    
    boolean isLeftChild =false;
    
    //busca el nodo a borrar 
    
   while(currentNode.key!=key){
   parentNode = currentNode;
   
   if(key<currentNode.key){
   isLeftChild =true;
   currentNode = currentNode.leftChild;
   }
   else{
   currentNode = currentNode.rightChild;
   
   isLeftChild=false ;
   }   
   if (currentNode ==null){
   return false;
   
   
   
   }
   }
    
   
   
   //para este punto ya  se encontro nodo
   
  Arbol nodeToDelete = currentNode;
    //si el nodo es tiene un hijo
    if(nodeToDelete.leftChild == null && nodeToDelete == null){
    if(nodeToDelete==root){
    root = null;
 }
    else if(isLeftChild){
    parentNode.leftChild=null;
    }
    else{
    parentNode.rightChild=null;
    
    
    }
    
    
    }
    
    
//si nodo tiene un hijo que esta a la izq
else if (nodeToDelete.rightChild == null){

if(nodeToDelete == root){
root=nodeToDelete.leftChild;

}
else if(isLeftChild){
parentNode.leftChild=nodeToDelete.leftChild;
}else{
parentNode.rightChild=nodeToDelete.leftChild;
}
}
//si nodo tiene un hijo que esta a la der
else if (nodeToDelete.leftChild == null){

if(nodeToDelete == root){
root=nodeToDelete.rightChild;

}
else if(isLeftChild){
parentNode.leftChild=nodeToDelete.rightChild;
}else{
parentNode.rightChild=nodeToDelete.rightChild;
}
}
    
    
    //si el nodo tiene 2 hijos 
    
//si el nodo tiene dos hijos 
else{
Arbol successor = getSuccessor(nodeToDelete);

//connect parent of nodeToDelete to successor instead
if(nodeToDelete == root){

root=successor;
} else if(isLeftChild){
parentNode.leftChild = successor;

} else{
parentNode.rightChild = successor;
}
successor.leftChild = nodeToDelete.leftChild;
}
    
    return true;
    
    }
    
    private Arbol getSuccessor(Arbol nodeToDelete){
    Arbol successorParent = nodeToDelete;
    Arbol successor =nodeToDelete;
    
    Arbol current = nodeToDelete.rightChild;
    
    while(current !=null){
    successorParent = successor;
    successor = current;
    current = current.leftChild;
    }
    
    if(successor != nodeToDelete.rightChild){
    successorParent.leftChild = successor.rightChild;
    successor.rightChild = nodeToDelete.rightChild;
    
    
    }
    
    return successor;
    
    
    }











}
